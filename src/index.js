import React from 'react';
import ReactDOM from 'react-dom';
import {createStore} from 'redux';
import {Provider} from 'react-redux';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';

import MainReducer from './store/reducer/MainReducer';
import InitialState from './store/state/InitialState';

import App from './component/app/App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';

const store = createStore(
    MainReducer,
    InitialState
);
injectTapEventPlugin();

ReactDOM.render(
    <Provider store={store}>
        <MuiThemeProvider>
            <App />
        </MuiThemeProvider>
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
