/**
 *
 *
 */
class ParsedDate {

    constructor(timestamp = null) {
        this.date = timestamp ? new Date(timestamp) : new Date();
    }

    _resolveDate(date = null) {
        if (date)
            return !date instanceof Date ? new Date(date) : date;
        else
            return this.date;

    }

    getGermanDate(date = null) {
        const dDate = this._resolveDate(date);

        return `${(dDate.getDate() < 10 ? "0":"")+dDate.getDate().toString()}.${(dDate.getMonth()+1 < 10 ? "0":"")+(dDate.getMonth()+1).toString()}.${dDate.getFullYear().toString()}`
    }

    getGermanTime(date = null) {
        const dDate = this._resolveDate(date);

        return `${(dDate.getHours() < 10 ? "0":"")+dDate.getHours().toString()}:${(dDate.getMinutes() < 10 ? "0":"")+dDate.getMinutes().toString()}`
    }

    getSqlTimestamp(date = null) {
        const dDate = this._resolveDate(date);

        return `${dDate.getFullYear().toString()}-${(dDate.getMonth()+1 < 10 ? "0":"")+(dDate.getMonth()+1).toString()}-${(dDate.getDate() < 10 ? "0":"")+dDate.getDate().toString()} ${(dDate.getHours() < 10 ? "0":"")+dDate.getHours().toString()}:${(dDate.getMinutes() < 10 ? "0":"")+dDate.getMinutes().toString()}:${(dDate.getSeconds() < 10 ? "0":"")+dDate.getSeconds().toString()}`
    }
}

export default ParsedDate;