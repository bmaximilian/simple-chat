/**
 * Created by m.beck on 23.05.2017.
 */
import ParsedDate from './ParsedDate';

it('can be created from empty date', () => {
    let date = new ParsedDate();
});

it('can be created from date', () => {
    let date = new ParsedDate(new Date('1970-01-01 12:00:00'));
});

it('can be created from miliseconds', () => {
    let date = new ParsedDate(1495545217140);
});

it('can be created from string', () => {
    let date = new ParsedDate('1970-01-01 12:00:00');
});