/**
 * The initial state of the chat application
 *
 * A Message should look like this:
 * {
 *     id: 2,
 *     text: "Welt",
 *     user: {
 *         username: "tester2",
 *         picture: "http://placehold.it/350x350"
 *     },
 *     time: "12:00"
 * }
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
export default {
    socket: null,
    connectedUsers: [],
    messages: [],
    typingUser: null,
    loggedInUser: null
}