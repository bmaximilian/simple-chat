import cloneDeep from 'lodash/cloneDeep';
import InitialState from '../state/InitialState';
import SocketModel from '../action/model/SocketModel';

/**
 * Reducer for socket
 *
 * @param state
 * @param action
 * @returns {*}
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
export default function socket(state = cloneDeep(InitialState), action) {
    switch (action.type) {
        case SocketModel.identifier: {
            return action.model.io
        }

        default: {
            return state;
        }
    }
}