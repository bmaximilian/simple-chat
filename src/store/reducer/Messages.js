import isArray from 'lodash/isArray';
import cloneDeep from 'lodash/cloneDeep';
import InitialState from '../state/InitialState';
import MessageModel from '../action/model/MessageModel';

/**
 * Message reducer function
 * Sets id of new message if not set
 * and assigns message to messages array in state
 *
 * @param state
 * @param action
 * @returns {*}
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
export default function messages(state = cloneDeep(InitialState), action) {
    switch (action.type) {
        case MessageModel.identifier: {

            if (!action.model.id && isArray(state))
                action.model.id = state.length === 0 ? 1 : state[state.length - 1].id - 1;

            return [...state, action.model]
        }

        default: {
            return state;
        }
    }
}