import {combineReducers} from 'redux';
import {connectedUsers, typingUser, loggedInUser} from './Users';
import messages from './Messages';
import socket from './Socket';

/**
 * Combines all reducers to one main reducer
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
export default combineReducers({
    socket,
    connectedUsers,
    messages,
    typingUser,
    loggedInUser
});