//import assign from 'lodash/assign';
import cloneDeep from 'lodash/cloneDeep';
import InitialState from '../state/InitialState';
import UserModel from '../action/model/UserModel';

/**
 * Reducer for connecting a user
 *
 * @param state
 * @param action
 * @returns {*}
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
export function connectedUsers(state = cloneDeep(InitialState), action) {
    switch (action.type) {
        case UserModel.connectedIdentifier: {
            return action.model.username ? [...state, action.model] : state
        }

        case UserModel.disconnectedIdentifier: {
            let i = 0;
            for (i; i < state.length; i++) {
                if (state[i].username === action.model.username && state[i].id === action.model.id)
                    break;
            }

            return [
                ...state.slice(0, i),
                ...state.slice(i+1)
            ]
        }

        default: {
            return state;
        }
    }
}

/**
 * Reducer to set a typing user
 *
 * @param state
 * @param action
 * @returns {*}
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
export function typingUser(state = cloneDeep(InitialState), action) {
    switch (action.type) {
        case UserModel.startTypingIdentifier: {
            //return assign({}, state, action.model)
            return action.model
        }

        case UserModel.endTypingIdentifier: {
            return (state.username === action.model.username || state.id === action.model.id) ? null : state
        }

        default: {
            return state;
        }
    }
}

/**
 * Reducer to set the logged in user
 *
 * @param state
 * @param action
 * @returns {*}
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
export function loggedInUser(state = cloneDeep(InitialState), action) {
    switch (action.type) {
        case UserModel.loggedInIdentifier: {
            return action.model
        }

        case UserModel.loggedOutIdentifier: {
            return state === action.model ? null : state
        }

        default: {
            return state;
        }
    }
}