import io from 'socket.io-client';
import SocketModel from '../model/SocketModel';

/**
 * Creates Socket if not already created
 *
 * @param socket
 * @returns {{type: string, model: SocketModel}}
 */
export function createSocket(socket = null) {
    if (!socket)
        socket = io.connect("http://localhost:8001");

    return {
        type: SocketModel.identifier,
        model: !socket instanceof SocketModel ? new SocketModel(socket) : socket
    }
}

/**
 * Sets socket to null
 *
 * @returns {{type: string, model: SocketModel}}
 */
export function destroySocket() {

    return {
        type: SocketModel.identifier,
        model: !new SocketModel(null)
    }
}