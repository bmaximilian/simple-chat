import MessageModel from '../model/MessageModel';

/**
 * Function to assign a message to the messages array
 *
 * @param text
 * @param user
 * @param id
 * @param fromServer
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
export function sendMessage(text, user, id = null, fromServer = false) {

    if (!fromServer) {
        
    }

    return {
        type: MessageModel.identifier,
        model: new MessageModel(text, user, id)
    }
}