import UserModel from '../model/UserModel';

/**
 * Function to connect a user to the chat
 *
 * @param user
 * @returns {{type: string, model: *}}
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
export function connectUser(user) {

    return {
        type: UserModel.connectedIdentifier,
        model: user instanceof UserModel ? user : new UserModel(user.username, user.picture, user.id)
    }
}

/**
 * Removes a user from the chat
 *
 * @param user
 * @returns {{type: string, model: *}}
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
export function disconnectUser(user) {

    return {
        type: UserModel.disconnectedIdentifier,
        model: user
    }

}

/**
 * Sets the typing user to the submitted user
 *
 * @param user
 * @returns {{type: string, model: *}}
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
export function startTyping(user) {
    return {
        type: UserModel.startTypingIdentifier,
        model: user
    }
}

/**
 * Removes the typing user
 *
 * @param user
 * @returns {{type: string, model: *}}
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
export function endTyping(user) {
    return {
        type: UserModel.endTypingIdentifier,
        model: user
    }
}

/**
 * Registers the user of the application
 *
 * @param user
 * @returns {{type: string, model: *}}
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
export function logIn(user) {
    return {
        type: UserModel.loggedInIdentifier,
        model: user instanceof UserModel ? user : new UserModel(user.username, user.picture, user.id)
    }
}


/**
 * Signs a user out
 *
 * @param user
 * @returns {{type: string, model: *}}
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
export function logOut(user) {
    return {
        type: UserModel.loggedOutIdentifier,
        model: user
    }
}