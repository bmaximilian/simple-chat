/**
 * Model for creating a user
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class UsersModel {

    /**
     * Constructor of users model
     *
     * @param username
     * @param picture
     * @param id
     */
    constructor(username, picture, id = null) {
        this.username = username;
        this.picture = picture;
        this.id = id;
    }

}

UsersModel.connectedIdentifier = "USER_CONNECTED";
UsersModel.disconnectedIdentifier = "USER_DISCONNECTED";
UsersModel.startTypingIdentifier = "USER_TYPING_START";
UsersModel.endTypingIdentifier = "USER_TYPING_END";
UsersModel.loggedInIdentifier = "USER_LOGGING_IN";
UsersModel.loggedOutIdentifier = "USER_LOGGING_OUT";

export default UsersModel;