import ParsedDate from '../../../classes/ParsedDate';

/**
 * Model for creating a message
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class MessageModel {

    /**
     * Constructor of MessageModel
     *
     * @param text
     * @param user
     * @param id
     */
    constructor(text, user, id = null) {
        this.text = text;
        this.user = user;
        this.id = id;
        this.time = new ParsedDate().getSqlTimestamp();
    }
}

MessageModel.identifier = "MESSAGE";

export default MessageModel