/**
 * Model for web socket
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class SocketModel {

    /**
     * constructor for socket model
     *
     * @param io
     */
    constructor(io) {
        this.io = io;
    }

}

SocketModel.identifier = "SOCKET";

export default SocketModel;