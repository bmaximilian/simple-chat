import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import InitialState from '../../store/state/InitialState';

import ChatHeader from './ChatHeader';
import ChatMessageList from './ChatMessageList';
import UserHint from './UserHint';
import ChatInput from './ChatInput';

import {sendMessage} from '../../store/action/creator/MessageCreator';
import {connectUser, disconnectUser, startTyping, endTyping, logIn, logOut} from '../../store/action/creator/UserCreator';
import {destroySocket} from '../../store/action/creator/SocketCreator';

/**
 * Main and stateful component of the chat application
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class Chat extends Component {

    /**
     * Constructor of chat component
     */
    constructor() {
        super();

        this.user = {
            username: "tester1"
        };
    }

    /**
     * Function called when component is mounted into the dom
     * Connects to the web socket
     */
    componentDidMount() {
        this.props.dispatch(logIn(this.user));
        this.props.dispatch(connectUser(this.props.state.loggedInUser ? this.props.state.loggedInUser : this.user));

        const io = this.props.state.socket;
        if (io) {
            /* Emit user connected event */
            io.emit("user connected", this.props.state.loggedInUser ? this.props.state.loggedInUser : this.user);

            /* TODO: get already connected users and already sent messages */

            /* Listen on user connected event */
            io.on("user connected", (user) => {
                if (user && user.username)
                    this.props.dispatch(connectUser(user));
            });

            /* Listen on user disconnected event */
            io.on("user disconnected", (user) => this.props.dispatch(disconnectUser(user)));

            /* Listen on user start typing event */
            io.on("user start typing", (user) => this.props.dispatch(startTyping(user)));

            /* Listen on user end typing event */
            io.on("user end typing", (user) => this.props.dispatch(endTyping(user)));

            /* Listen on user send message event */
            io.on("user send message", (message) => this.props.dispatch(sendMessage(message.text, message.user, message.id, true)));
        }
    }

    /**
     * Function called when component will unmount from the dom
     */
    componentWillUnmount() {
        /* Destroy socket connection */
        if (this.props.socket.disconnect()) {
            this.props.dispatch(disconnectUser(this.props.state.loggedInUser));
            this.props.dispatch(logOut(this.props.state.loggedInUser));
            this.props.dispatch(destroySocket());
        }
    }

    /**
     * Function called when user clicks on send button
     *
     * @returns {Function}
     */
    sendMessage() {
        return function(text) {
            /* Emit web socket user send message event with message */
            this.props.state.socket.emit("user send message", {text: text, user: this.props.state.loggedInUser, id: null});
            this.props.dispatch(sendMessage(text, this.props.state.loggedInUser));

            setTimeout(() => {
                var elem = document.getElementById('chat-list');
                elem.scrollTop = elem.scrollHeight;
            }, 250);
        }
    }

    /**
     * Returns function called when user starts typing
     *
     * @returns {function(): *}
     */
    startTyping() {
        /* Emit user start typing event with user */
        return () => this.props.state.socket.emit("user start typing", this.props.state.loggedInUser);
        //return () => this.props.dispatch(startTyping(this.props.state.loggedInUser));
    }

    /**
     * Returns function when user stops typing
     *
     * @returns {function(): *}
     */
    endTyping() {
        /* Emit user end typing event with user */
        return () => this.props.state.socket.emit("user end typing", this.props.state.loggedInUser);
        //return () => this.props.dispatch(endTyping(this.props.state.loggedInUser));
    }

    /**
     * Renders the Component
     *
     * @returns {XML}
     */
    render() {
        console.log(this.props.state);
        return (
            <div className="chat">
                <ChatHeader room="Chat" usersConnected={this.props.state.connectedUsers.length} activeUser={this.props.state.loggedInUser} />
                <ChatMessageList messages={this.props.state.messages } activeUser={this.props.state.loggedInUser} />
                <UserHint typingUser={this.props.state.typingUser} />
                <ChatInput
                    onSendClick={this.sendMessage().bind(this)}
                    onTypeStart={this.startTyping().bind(this)}
                    onTypeEnd={this.endTyping().bind(this)}
                />
            </div>
        )
    }
}

Chat.propTypes = {
    state: PropTypes.object,
    dispatch: PropTypes.func
};

export default connect(
    (state) => ({state: state || InitialState})
)(Chat);