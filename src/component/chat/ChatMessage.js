import React from 'react';
import PropTypes from 'prop-types';
import ParsedDate from '../../classes/ParsedDate';

/**
 * Component for one chat message
 *
 * @param props
 * @constructor
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
const ChatMessage = (props) => (
    <li className={`message ${props.user.username === props.activeUser.username ? "self": "other"}`}>
        <div className="avatar">
            <img src={props.user.picture ? props.user.picture : "http://placehold.it/32x32"} alt={`user ${props.user.username}`} />
        </div>
        <div className="msg">
            <p>{props.text}</p>
            <time>{new ParsedDate(props.time).getGermanTime()}</time>
        </div>
    </li>
);

ChatMessage.propTypes = {
    user: PropTypes.object.isRequired,
    text: PropTypes.string.isRequired
};

ChatMessage.defaultProps = {
    user: {
        username: "",
        picture: "http://placehold.it/32x32"
    },
    text: ""
};

export default ChatMessage;