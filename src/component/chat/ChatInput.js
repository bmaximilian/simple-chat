import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import Send from 'material-ui/svg-icons/content/send';
import './css/ChatInput.css';

/**
 * Component for typing and sending a message
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class ChatInput extends Component {

    /**
     * Function to send the message and reset the input
     */
    sendMessage() {
        this.props.onSendClick(this.refs.message.input.value);
        this.refs.message.input.value = "";
    }

    /**
     * Renders the ChatInput component
     *
     * @returns {XML}
     */
    render() {
        return (
            <div className="chat-input">
                <TextField
                    className="input-message"
                    floatingLabelText="Message"
                    fullWidth={true}
                    ref="message"
                    onKeyPress={(e) => {if (e.which === 13 || e.keyCode === 13) this.sendMessage()}}
                    onFocus={this.props.onTypeStart}
                    onBlur={this.props.onTypeEnd}
                />

                <FlatButton
                    className="input-button"
                    label="Send"
                    labelPosition="before"
                    icon={<Send />}
                    primary={true}
                    onTouchTap={this.sendMessage.bind(this)}
                />
            </div>
        );
    }
}

export default ChatInput;