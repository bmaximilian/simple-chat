import React from 'react';
import PropTypes from 'prop-types';
import ChatMessage from './ChatMessage';
import './css/ChatMessageList.css';

/**
 * List containing all messages of the chat
 *
 * @param props
 * @constructor
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
const ChatMessageList = (props) => (
    <ol className="chat chat-messages custom-scrollbar" id="chat-list">
        {props.messages.map(message =>
            <ChatMessage key={message.id} text={message.text} user={message.user} time={message.time} activeUser={props.activeUser} />
        )}
    </ol>
);

ChatMessageList.propTypes = {
    messages: PropTypes.array.isRequired
};

ChatMessageList.defaultProps = {
    messages: []
};

export default ChatMessageList;