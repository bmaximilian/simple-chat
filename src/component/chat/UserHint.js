import React from 'react';
import PropTypes from 'prop-types';
import './css/UserHint.css';

/**
 * Component for showing hint texts
 *
 * @param props
 * @constructor
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
const UserHint = (props) => (
    <div className={`user-hint ${props.typingUser != null ? "active" : ""}`}>
        <small className="input-hint">{props.typingUser != null ? props.typingUser.username + " is typing..." : ""}</small>
    </div>
);

UserHint.propTypes = {
    typingUser: PropTypes.object
};

UserHint.defaultProps = {
    typingUser: null
};

export default UserHint;