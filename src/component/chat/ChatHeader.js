import React from 'react';
import PropTypes from 'prop-types';
import './css/ChatHeader.css';

/**
 * Header displaying general informations about the chat
 *
 * @param props
 * @constructor
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
const ChatHeader = (props) => (
    <div className="chat-header">
        <div className="subheader-container">
            <div className="subheader left">
                <small>user: <strong>{props.activeUser ? props.activeUser.username : "no user"}</strong></small>
            </div>
            <div className="subheader">
                <small>connected users: <strong>{props.usersConnected}</strong></small>
            </div>
        </div>
        <h1>{props.room}</h1>
    </div>
);

ChatHeader.propTypes = {
    room: PropTypes.string,
    usersConnected: PropTypes.number
};

ChatHeader.defaultProps = {
    room: "Chat",
    usersConnected: 0
};

export default ChatHeader;