import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import InitialState from '../../store/state/InitialState';
import logo from '../../logo.svg';
import './css/App.css';
import Chat from '../chat/Chat';

import {createSocket} from '../../store/action/creator/SocketCreator';

/**
 * Main component of the app
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class App extends Component {
    
    componentDidMount() {
        this.props.dispatch(createSocket(this.props.state.socket))
    }
    
    /**
     * Renders the app
     * 
     * @returns {XML}
     */
    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h2>React Chat</h2>
                </div>
                <p className="App-intro">
                    This is a simple <i>chat application</i> with <strong>React, Redux</strong> and <strong>Material-UI</strong>.
                </p>
                <Chat />
            </div>
        );
    }
}

App.propTypes = {
    state: PropTypes.object,
    dispatch: PropTypes.func
};

export default connect(
    (state) => ({state: state || InitialState})
)(App);
